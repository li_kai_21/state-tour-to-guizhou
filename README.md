# 州游黔行

#### 介绍
{**州游黔程介绍**
本段代码使用unicloud开发，用户是基于uni-id用户体系创建，数据获取为jql语法，数据库与云函数使用阿里云，需自行购买以及配置}

#### 软件架构
软件架构说明
分为首页、文章编辑页、个人中心页、列表页、详情页等常规页面
没有后端，所有数据库操作均由前端完成


#### 安装教程

1.  下载到Hbuilder中 配置自己的 appid 
2.  以及unicloud购买云服务（本人使用的是阿里云免费的那个）
3.  Hbuilder可能会安装一些依赖，重启即可
4.  其他问题不知

#### 使用说明

暂无（遇到问题可以联系qq群：964204436）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
