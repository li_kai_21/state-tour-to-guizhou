// 向外导出富文本中插入的img的src地址，（采用正则
export function getImeSrc(richtext, num) {
	let imgList = [];
	richtext.replace(/<img [^>]*src=['"]([^'"]+)[^>]*>/g, (match, capture) => {
		imgList.push(capture);
	});
	imgList = imgList.slice(0, num)
	return imgList;
};
//  向外导出城市名称
export function getProvince() {
	return new Promise((resolve, reject) => {
		let historyCity = uni.getStorageSync('historyCity')
		// console.log(historyCity, 999);
		if (historyCity) {
			// console.log('走缓存', historyCity);
			if ((Date.now() - historyCity.time) > 1000 * 60 * 60) {
				getIp().then(res => {
					// console.log(res);
					resolve(res)
				}).catch(err => {
					reject(err)
				})
			} else {
				// console.log('缓存', historyCity.province);
				resolve(historyCity.province)
			}
		} else {
			// console.log('第一次网络');
			getIp().then(res => {
				// console.log(res);
				resolve(res)
			}).catch(err => {
				reject(err)
			})
		}
	})
};

// 通过ip地址向高德获取城市名称
function getIp() {
	return new Promise((resolve, reject) => {
		uni.request({
			url: "https://restapi.amap.com/v3/ip?key=1e89754c31e88198c1eb145985fce8c5",
			success: res => {
				// console.log('网络侵权');
				let str = ''
				if (typeof(res.data.city) == 'string') {
					str = res.data.city;
				} else if (typeof(res.data.city) == '') {
					str = '火星'
				} else {
					str = '火星'
				}
				resolve(str)
				let obj = {
					province: str,
					time: Date.now()
				}
				uni.setStorageSync('historyCity', obj);
			},
			fail: err => {
				reject(err)
			}
		})
	})
};
