'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	// const collection = db.collection("users")
	// let res = await collection.get()
	// let res = await db.collection('users').count()
	let {
		name,
		tel
	} = event;
	let res = await db.collection("users").add({
		name,
		tel
	})
	return res;
};
