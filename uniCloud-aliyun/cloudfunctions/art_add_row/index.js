const db = uniCloud.database()
exports.main = async (event, context) => {
	let {
		detail,
		picurls
	} = event;

	return db.collection('article').add({
		posttime: Date.now(),
		picurls,
		...detail
	})
};
