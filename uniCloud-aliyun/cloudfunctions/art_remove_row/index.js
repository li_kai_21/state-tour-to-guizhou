const db = uniCloud.database()
exports.main = async (event, context) => {
	let {
		id
	} = event;
	// 进行删除操作时必须不要.doc()
	return await db.collection('article').doc(id).remove()
};
